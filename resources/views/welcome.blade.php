<!DOCTYPE html>
<html>
    <head>
        <title>Redux Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Redux Laravel App</div>
                <div id="root">Redux Laravel App</div>
            </div>
        </div>

        <!--
            Setup paths and other stuff we need to know inside the JS coming from PHP
            TODO :: use PHP_VARS_TO_JS package to do this
        -->
        @include('partials.jsbootstrap')

        <!--
            Add JS here
            This will become a one point entry for my webpack stuff (later)
        -->
        <script src="{{ url('js/bundle.js') }}"></script>

    </body>
</html>
