<script>
	var baseUrlTmp = <?php echo json_encode(url('')); ?>;
	var environment = <?php echo json_encode(App::environment()); ?>;
	var bootstrappedData = {
		baseUrl: baseUrlTmp,
		env: environment
	};
</script>
