let baseUrl = 'http://reduxlaravel.dev';
if ( typeof bootstrappedData !== typeof undefined ) {
    baseUrl = bootstrappedData.baseUrl;
}

export default baseUrl;
