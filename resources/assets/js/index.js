import React from 'react';
import { render } from 'react-dom';
import { App } from './components/App';
import baseUrl from './config';
import css from './assets/app.less';

render(<App baseUrl={baseUrl}/>, document.getElementById('root'));
