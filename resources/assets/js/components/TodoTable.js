import React from 'react';
import TodoItem from './TodoItem';

const TodoTable = React.createClass({
    renderTodo(todo) {
        return <TodoItem key={todo.id} todo={todo} />
    },

    render() {
        const {todos} = this.props;
        return (
            <table className="table table-striped table-hover table-condensed">
                <thead>
                    <tr>
                        <th className="id">ID</th>
                        <th className="customer">customer</th>
                        <th className="text">text</th>
                        <th className="note">note</th>
                    </tr>
                </thead>
                <tbody>
                    {todos.map(this.renderTodo)}
                </tbody>
            </table>
        );
    }
});

export default TodoTable;
