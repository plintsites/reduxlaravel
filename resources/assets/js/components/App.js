import React from 'react';
import TodoTable from './TodoTable';
import $ from 'jquery';

export const App = React.createClass({
    getInitialState() {
        return {
            todos: [],
            sortCol: 'id'
        };
    },

    componentDidMount() {
        // save 'this' for use inside the done callback below
        const saveThis = this;

        // Read initial state using fetch API

        fetch(this.props.baseUrl + '/api/todos')
          .then(
            function(response) {
              if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                  response.status);
                return;
              }

              // Examine the text in the response
              response.json().then(function(todos) {
                saveThis.setState({
                    todos
                });
              });
            }
          )
          .catch(function(err) {
            console.log('Fetch Error :-S', err);
          });
    },

    render() {
        return (
            <div>
                <h1>My new TodoTable example app</h1>
                <p>
                    Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het merendeel
                    heeft te lijden gehad van wijzigingen in een of andere vorm, door ingevoegde humor
                    of willekeurig gekozen woorden die nog niet half geloofwaardig ogen. Als u een
                    passage uit Lorum Ipsum gaat gebruiken dient u zich ervan te verzekeren dat er niets
                    beschamends midden in de tekst verborgen zit.
                </p>
                <TodoTable todos={this.state.todos} />
            </div>
        );
    }
});
