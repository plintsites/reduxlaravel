import React from 'react';

const TodoItem = React.createClass({
    render() {
        const {todo} = this.props;
        return (
            <tr>
                <td className="id">{todo.id}</td>
                <td className="customer">{todo.customer}</td>
                <td className="text">{todo.text}</td>
                <td className="note">{todo.note}</td>
            </tr>
        );
    }
});

export default TodoItem;
