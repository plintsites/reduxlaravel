<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');        // de user die het moet uitvoeren
            $table->string('customer');        // de klant voor wie het moet gebeuren
            $table->string('text', 255);       // todo tekst
            $table->text('note');              // extra opmerking
            $table->decimal('duration', 8, 2); // ingeschatte tijd
            $table->tinyInteger('completed');  // afgerond of niet
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('todos');
    }
}
