<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Todo::class, function (Faker\Generator $faker) {
    return [
        'user_id' => rand(1,5),
        'customer' => $faker->numerify('Customer ###'), // 'Customer 609'
        'text' => $faker->sentence($nb=5, $variableNbWords = true),
        'note' => $faker->sentence($nb=20, $variableNbWords = true),
        'duration' => $faker->randomFloat($nbMaxDecimals = 1, $min = 0.5, $max = 8),
        'completed' => rand(0,1)
    ];
});
