<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('TodoTableSeeder');
        $this->command->info('Todo table seeded');

        Model::reguard();
    }
}

class TodoTableSeeder extends Seeder {

    public function run() {

        DB::table('todos')->delete();

        $todo = factory(App\Todo::class, 500)->create();

    }

}
